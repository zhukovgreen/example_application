"""Example entrypoints."""
from typing import Any

import click


def like(*args: Any, **kwargs: Any) -> None:
    """Like a song."""
    click.echo("You just liked a song with args {!r} and kwargs {!r}".format(args, kwargs))


def unlike(*args: Any, **kwargs: Any) -> None:
    """Unlike a song."""
    click.echo("You just liked a song with args {!r} and kwargs {!r}".format(args, kwargs))


def follow(*args: Any, **kwargs: Any) -> None:
    """Follow an artist."""
    click.echo("You just liked a song with args {!r} and kwargs {!r}".format(args, kwargs))


def add_to_favorite(*args: Any, **kwargs: Any) -> None:
    """Add a song to a favorite list."""
    click.echo("You just liked a song with args {!r} and kwargs {!r}".format(args, kwargs))
