# Setup environment
1. pip3.6 install pipenv
1. pipenv install --dev

# Run tests
1. Setup environment
1. PYTHONAPATH=. pipenv run pytest

# Interface
1. Setup environment
1. pipenv run python cli.py make_an_action --help 
