from typing import Callable

from click.testing import CliRunner


def test_cli(make_an_action: Callable) -> None:
    runner = CliRunner()

    # test normal behavior + verbosity
    result = runner.invoke(make_an_action, ['-v', 'like'])
    assert result.exit_code == 0
    assert 'liked' in result.output

    # test bad action
    result = runner.invoke(make_an_action, ['-v', 'kill_them'])
    assert 'Error:' in result.output
