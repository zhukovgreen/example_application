from typing import Callable

import pytest

import cli


@pytest.fixture(scope='session')
def make_an_action() -> Callable:
    return cli.make_an_action
