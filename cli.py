from typing import Any

import click

from example_project import like, unlike, follow, add_to_favorite


@click.group()
def cli() -> None:
    pass


@cli.command(context_settings=dict(allow_extra_args=True, ignore_unknown_options=True))
@click.option('--verbose', '-v', count=True)
@click.argument('action')
@click.pass_context
def make_an_action(ctx: Any, verbose: int, action: str) -> None:
    if verbose:
        click.echo(f'Verbosity level set on {verbose}')
    command_mapping: dict = {
        'like': like,
        'unlike': unlike,
        'follow': follow,
        'add_to_favorite': add_to_favorite,
    }
    if action not in command_mapping:
        raise click.ClickException(f'{action} not in available actions {list(command_mapping.keys())}')

    # finally invoking a call
    return command_mapping[action](*ctx.args)


if __name__ == '__main__':
    cli()
